import { gql } from '@apollo/client'

const getStationsQuery = gql`
    query Stations($input: StationsInput!) {
        stations(input: $input) {
            id
            name
        }
    }
`;

export default getStationsQuery
