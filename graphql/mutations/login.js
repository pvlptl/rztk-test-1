import { gql } from '@apollo/client'

const loginMutation = gql`
    mutation Login($email: String!, $password: String!) {
        login(input: { email: $email, password: $password }) {
            user {
                id
                email
            }
        }
    }
`;

export default loginMutation
