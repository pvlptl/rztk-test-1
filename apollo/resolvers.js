import {ApolloError, AuthenticationError, UserInputError} from 'apollo-server-micro'
import {findUser, validatePassword} from '../lib/user'
import {getLoginSession, setLoginSession} from '../lib/auth'
import {removeTokenCookie} from '../lib/auth-cookies'

export const resolvers = {
    Query: {
        async me(_parent, _args, context, _info) {
            try {
                const session = await getLoginSession(context.req)

                if (session) {
                    return findUser({ email: session.email })
                }
            } catch (error) {
                throw new AuthenticationError(error)
            }
        },
        async stations(_parent, _args, context, _info) {
            try {

                const { limit, lat, lng } = _args.input

                return await fetch(process.env.STATIONS_API_URL, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                    },
                    body: JSON.stringify({
                        query: `{
                station: bikeStation(
                findBy: { closest: { latitude: ${lat}, longitude: ${lng} } }
                ) {
                ... on BikeStation {
                name
                coordinates {
                longitude
                latitude
                }
                available {
                bikes {
                electrical
                mechanical
                }
                }
                }
                }
                stations: metroStations(filterBy: { lineId: 4 }, first: ${limit}) {
                edges {
                node {
                id
                name
                }
                }
                }
                }`
                    })
                })
                    .then(r => r.json())
                    .then(res => res.data.stations.edges.map(edge => edge.node))
            } catch (error) {
                throw new ApolloError(error)
            }
        },
    },
    Mutation: {
        async login(_parent, args, context, _info) {
            const user = await findUser({ email: args.input.email })

            if(!user) {
                throw new UserInputError('User not found')
            }

            const isPasswordValid = await validatePassword(user, args.input.password)

            if(!isPasswordValid) {
                throw new UserInputError('Password not valid')
            }

            const session = {
                id: user.id,
                email: user.email,
            }

            await setLoginSession(context.res, session)

            return { user }
        },

        async logout(_parent, _args, context, _info) {
            removeTokenCookie(context.res)
            return true
        },
    },
}