import { gql } from '@apollo/client'

export const typeDefs = gql`
    type User {
        id: ID!
        email: String!
    }

    type Station {
        id: ID!
        name: String!
    }

    input LoginInput {
        email: String!
        password: String!
    }

    input StationsInput {
        limit: Int!
        lat: Float!
        lng: Float!
    }

    type LoginPayload {
        user: User!
    }

    type Query {
        me: User
        stations(input: StationsInput!): [Station]!
    }

    type Mutation {
        login(input: LoginInput!): LoginPayload!
        logout: Boolean!
    }
`