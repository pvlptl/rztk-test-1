import {useMemo} from 'react'
import {ApolloClient, InMemoryCache, createHttpLink} from '@apollo/client'
import merge from 'deepmerge'
import isClient from "../utils/isClient";
import { setContext } from '@apollo/client/link/context';
import schema from "./schema";

let apolloClient

function createLink({ ctx = {} }) {
    if (!isClient) {
        const { SchemaLink } = require('@apollo/client/link/schema')
        return new SchemaLink({
            schema,
            context: context => ({
                ...context,
                ...ctx
            })
        })
    } else {
        const httpLink = createHttpLink({
            uri: process.env.NEXT_PUBLIC_API,
        });

        const authLink = setContext(() => {
            return {
                headers: {
                    ...ctx.req?.headers
                }
            }
        });

        return authLink.concat(httpLink)
    }
}

function createApolloClient({ ctx }) {
    return new ApolloClient({
        credentials: 'same-origin',
        ssrMode: !isClient,
        link: createLink({ ctx }),
        cache: new InMemoryCache(),
    })
}

export function initializeApollo(initialState = null, ctx) {
    const _apolloClient = apolloClient ?? createApolloClient({ ctx })

    if (initialState) {

        const existingCache = _apolloClient.extract()

        const data = merge(initialState, existingCache)

        _apolloClient.cache.restore(data)
    }

    if (!isClient) return _apolloClient

    if (!apolloClient) apolloClient = _apolloClient

    return _apolloClient
}

export function useApollo(initialState) {
    return useMemo(() => initializeApollo(initialState), [initialState])
}