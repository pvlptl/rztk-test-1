import { ApolloProvider } from '@apollo/client'
import { useApollo } from '../apollo/client'
import CssBaseline from '@mui/material/CssBaseline';
import '../styles/global.css'
import { SnackbarProvider } from 'notistack';

export default function App({ Component, pageProps }) {
    const apolloClient = useApollo(pageProps.initialApolloState)
    return (
        <ApolloProvider client={apolloClient}>
            <CssBaseline />
            <SnackbarProvider maxSnack={3}>
                <Component {...pageProps} />
            </SnackbarProvider>
        </ApolloProvider>
    )
}