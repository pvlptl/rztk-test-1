import HomeForm from "../components/HomeForm";
import HomeFormHint from "../components/HomeFormHint";
import Box from "@mui/material/Box";
import useHomeForm from "../hooks/useHomeForm";

function Home() {
    const { onChange, onSubmit, loading, values, onSetValues } = useHomeForm()

    const handlePasteUserCredentials = number => {
        switch (number) {
            case 1:
                return onSetValues({
                    email: 'qwerty@gmail.com',
                    password: 'qwerty',
                })
            case 2:
                return onSetValues({
                    email: 'user@gmail.com',
                    password: 'user',
                })
        }
    }

    const hints = [
        {
            id: 1,
            onClick: () => handlePasteUserCredentials(1),
            email: 'qwerty@gmail.com',
            password: 'qwerty',
        },
        {
            id: 2,
            onClick: () => handlePasteUserCredentials(2),
            email: 'user@gmail.com',
            password: 'user',
        }
    ]

    return (
        <Box sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            height: '100%'
        }}>
            <Box sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                flexDirection: "column",
                width: '100%',
                maxWidth: 400
            }}>
                <h3>ТЗ Frontend developer (react) Senior</h3>

                <HomeForm
                    onChange={onChange}
                    onSubmit={onSubmit}
                    values={values}
                    loading={loading}
                />

                <HomeFormHint hints={hints} />
            </Box>
        </Box>
    )
}

export default Home
