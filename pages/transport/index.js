import getStationsQuery from "../../graphql/queries/getStations";
import TransportUI from "../../components/TransportUI";
import { initializeApollo} from "../../apollo/client";
import meQuery from "../../graphql/queries/me";

export default function Transport({ coordinates }) {
    return <TransportUI initCoordinates={coordinates}/>
}

export async function getServerSideProps(ctx) {
    const apolloClient = initializeApollo(null, ctx)

    const lat = parseFloat(ctx.query.lat)
    const lng = parseFloat(ctx.query.lng)

    let promises = [
        apolloClient.query({
            query: meQuery,
        })
    ]

    if(lat && lng) {
        promises.push(apolloClient.query({
            query: getStationsQuery,
            variables: {
                input: {
                    lat,
                    lng,
                    limit: 25
                }
            }
        }))
    }

    await Promise.all(promises);

    return {
        props: {
            initialApolloState: apolloClient.cache.extract(),
            coordinates: {
                lat,
                lng,
            }
        },
    }
}