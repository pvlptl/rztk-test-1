import Rows from "./Rows";
import useTransportForm from "../hooks/useTransportForm";
import TransportHeader from "./TransportHeader";
import TransportForm from "./TransportForm";
import useMe from "../hooks/useMe";
import useLogout from "../hooks/useLogout";
import useRedirectIfGuestWatcher from "../hooks/useRedirectIfGuestWatcher";
import {Alert, Box, Button} from "@mui/material";
import useRows from "../hooks/useRows";

function TransportUI({ initCoordinates = {} }) {

    useRedirectIfGuestWatcher()

    const me = useMe()
    const logoutHook = useLogout()
    const { rows, loading } = useRows()


    const {
        onReset,
        onSubmit,
        onChange,
        coordinates
    } = useTransportForm({
        initCoordinates
    })

    const hasCoordinates = coordinates.lat && coordinates.lng

    return (
        <Box sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            flexDirection: "column",
        }}>
            {me.user && (
                <TransportHeader
                    onLogout={logoutHook.onLogout}
                    name={me.user?.email}
                    logoutLoading={logoutHook.loading}
                />
            )}

            <Box sx={{
                width: '100%',
                maxWidth: 400,
                marginTop: theme => theme.spacing(3)
            }}>
                <TransportForm
                    disabled={!hasCoordinates || loading}
                    onSubmit={onSubmit}
                    onChange={onChange}
                    values={coordinates}
                    loading={loading}
                />

                {rows.length > 0 && (
                    <>
                        <Button sx={{ width: '100%', marginTop: theme => theme.spacing(3) }} variant="outlined" onClick={onReset}>Reset</Button>
                        <Rows rows={rows} />
                    </>
                )}

                {!loading && rows.length === 0 && (
                    <Alert severity="warning" sx={{ marginTop: theme => theme.spacing(3) }}>Please select coordinates to see rows</Alert>
                )}
            </Box>
        </Box>
    )
}

export default TransportUI