import Row from "./Row";
import Box from '@mui/material/Box';
import List from '@mui/material/List';

function Rows({rows = []}) {
    return (
        <Box sx={{ width: '100%', bgcolor: 'background.paper' }}>
            <nav aria-label="main mailbox folders">
                <List>
                    {rows.map(row => <Row key={row.id} name={row.name} />)}
                </List>
            </nav>
        </Box>
    )
}

export default Rows