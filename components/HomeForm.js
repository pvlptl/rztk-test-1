import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';

function HomeForm({ onSubmit, onChange, values, loading }) {
    return (
        <form style={{
            display: 'flex',
            flexDirection: 'column',
            gap: 24,
            width: '100%'
        }} onSubmit={onSubmit}>
            <TextField
                label="Email"
                variant="outlined"
                value={values.email}
                onChange={onChange}
                name="email"
                type="email"
                autoComplete="email"
                required
            />
            <TextField
                label="Password"
                variant="outlined"
                value={values.password}
                onChange={onChange}
                name="password"
                type="password"
                autoComplete="password"
                required
            />
            <Button
                disabled={loading}
                type="submit"
                variant="contained"
            >
                {loading ? 'Loading...' : 'Login'}
            </Button>
        </form>
    )
}

export default HomeForm