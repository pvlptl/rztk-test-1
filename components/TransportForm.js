import {Button, TextField} from "@mui/material";

function TransportForm({ onSubmit, onChange, values = {}, loading, disabled }) {
    return (
        <form onSubmit={onSubmit} style={{
            display: 'flex',
            flexDirection: 'column',
            gap: 24,
        }}>
            <TextField
                label="Latitude"
                value={values.lat}
                onChange={onChange}
                step="any"
                type="number"
                name="lat"
                placeholder="Enter Latitude"
            />
            <TextField
                label="Longitude"
                value={values.lng}
                onChange={onChange}
                step="any" type="number"
                name="lng"
                placeholder="Enter Longitude"
            />
            <Button type="submit" variant="contained" disabled={disabled}>{loading ? 'Loading...' : 'Search'}</Button>
        </form>
    )
}

export default TransportForm