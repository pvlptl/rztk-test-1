import Button from "@mui/material/Button";
import Alert from '@mui/material/Alert';
import Stack from '@mui/material/Stack';
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";

function HomeFormHint({ hints = [] }) {
    return (
        <Box sx={{
            marginTop: theme => theme.spacing(3),
            width: '100%'
        }}>
            <Typography sx={{ marginBottom: theme => theme.spacing(2) }}>Test users:</Typography>
            <Stack sx={{ width: '100%' }} spacing={2}>
                {hints.map(hint => (
                    <Alert key={hint.id} severity="info">
                        <Typography sx={{ marginBottom: theme => theme.spacing(2) }}>{hint.email} - {hint.password}</Typography>
                        <Button
                            type="submit"
                            variant="outlined"
                            onClick={hint.onClick}
                        >
                            Paste credentials
                        </Button>
                    </Alert>
                ))}
            </Stack>
        </Box>
    )
}

export default HomeFormHint