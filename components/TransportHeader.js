import Button from "@mui/material/Button";
import {Alert} from "@mui/material";

function TransportHeader({ name, onLogout, logoutLoading }) {
    return (
        <Alert sx={{ width: '100%' }} severity="info">
            <h1>Вы вошли как: {name}</h1>
            <Button color="info" variant="outlined" disabled={logoutLoading} onClick={onLogout}>Logout</Button>
        </Alert>
    )
}

export default TransportHeader