import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import Icon from '@mui/icons-material/Extension';
import ListItemText from "@mui/material/ListItemText";
import Divider from "@mui/material/Divider";

function Row({ name }) {
    return (
        <>
            <ListItem disablePadding>
                <ListItemButton>
                    <ListItemIcon>
                        <Icon />
                    </ListItemIcon>
                    <ListItemText primary={name} />
                </ListItemButton>
            </ListItem>
            <Divider />
        </>
    )
}

export default Row