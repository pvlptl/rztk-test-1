import {v4 as uuidv4} from 'uuid'
import crypto from 'crypto'

const createUserHelper = (password) => {
    const salt = crypto.randomBytes(16).toString('hex')

    const hash = crypto
        .pbkdf2Sync(password, salt, 1000, 64, 'sha512')
        .toString('hex')

    return {
        id: uuidv4(),
        salt,
        hash,
    }
}

const users = [
    {
        email: 'qwerty@gmail.com',
        ...createUserHelper('qwerty')
    },
    {
        email: 'user@gmail.com',
        ...createUserHelper('user')
    }
]

export async function findUser({ email }) {
    return users.find((user) => user.email === email)
}

export async function validatePassword(user, inputPassword) {
    const inputHash = crypto
        .pbkdf2Sync(inputPassword, user.salt, 1000, 64, 'sha512')
        .toString('hex')
    return user.hash === inputHash
}