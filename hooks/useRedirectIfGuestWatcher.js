import Router from "next/router";
import useMe from "./useMe";
import {useEffect} from "react";

function useRedirectIfGuestWatcher() {
    const me = useMe()

    useEffect(() => {
        if(!me.loading && !me.user) {
            Router.push({
                pathname: '/',
                query: '',
            });
        }
    }, [me.loading, me.user])
}

export default useRedirectIfGuestWatcher