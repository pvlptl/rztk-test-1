import {useQuery} from "@apollo/client";
import meQuery from "../graphql/queries/me";

function useMe() {
    const {
        data,
        loading
    } = useQuery(meQuery)

    return {
        user: data?.me,
        loading
    }
}

export default useMe