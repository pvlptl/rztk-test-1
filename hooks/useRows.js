import {useQuery} from "@apollo/client";
import getStationsQuery from "../graphql/queries/getStations";
import {useRouter} from "next/router";

function useRows() {
    const router = useRouter()

    const {data: stationsData, loading} = useQuery(getStationsQuery, {
        variables: {
            input: {
                limit: 25,
                lat: parseFloat(router.query.lat),
                lng: parseFloat(router.query.lng),
            }
        },
        skip: !router.query.lat || !router.query.lng
    });

    const rows = stationsData?.stations || []

    return {
        loading,
        rows
    }
}

export default useRows