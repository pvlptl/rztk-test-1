import Router from "next/router";
import { useState } from "react";
import {useSnackbar} from "notistack";
import {getErrorMessage} from "../lib/form";

function useTransportForm({ initCoordinates = {}, onReset }) {
    const { enqueueSnackbar } = useSnackbar();
    const [coordinates, setCoordinates] = useState(initCoordinates)

    const handleReset = () => {
        setCoordinates({
            lat: '',
            lng: ''
        })

        Router.push({
            pathname: Router.pathname,
            query: '',
        }, undefined, { shallow: true });

        if(onReset) {
            onReset()
        }
    }

    const handleSubmit = async (e) => {
        try {
            e.preventDefault()

            Router.push({
                pathname: Router.pathname,
                query: coordinates,
            }, undefined, { shallow: true });
        } catch (error) {
            enqueueSnackbar(getErrorMessage(error), { variant: 'error' });
        }
    }

    const handleChange = e => {
        setCoordinates(prev => ({
            ...prev,
            [e.target.name]: parseFloat(e.target.value)
        }))
    }

    return {
        onReset: handleReset,
        onSubmit: handleSubmit,
        onChange: handleChange,
        coordinates
    }
}

export default useTransportForm