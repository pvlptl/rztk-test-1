import {useMutation} from "@apollo/client";
import logoutMutation from "../graphql/mutations/logout";
import {useApollo} from "../apollo/client";
import {useSnackbar} from "notistack";
import {getErrorMessage} from "../lib/form";

function useLogout() {
    const { enqueueSnackbar } = useSnackbar();
    const apolloClient = useApollo()
    const [mutate, { loading }] = useMutation(logoutMutation)

    const handleLogout = async () => {
        try {
            const res = await mutate()
            const success = res.data.logout

            if(success) {
                apolloClient.resetStore()
                enqueueSnackbar('Success', { variant: 'warning' });
            }

        } catch (error) {
            enqueueSnackbar(getErrorMessage(error), { variant: 'error' });
        }
    }

    return {
        loading,
        onLogout: handleLogout
    }
}

export default useLogout