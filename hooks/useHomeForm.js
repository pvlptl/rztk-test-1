import {getErrorMessage} from "../lib/form";
import {useSnackbar} from "notistack";
import {useApolloClient, useMutation} from "@apollo/client";
import loginMutation from "../graphql/mutations/login";
import {useRouter} from "next/router";
import {useState} from "react";

const initValues = {
    email: '',
    password: '',
}

function useHomeForm() {
    const { enqueueSnackbar } = useSnackbar();
    const client = useApolloClient()
    const [signIn, { loading }] = useMutation(loginMutation)
    const router = useRouter()
    const [values, setValues] = useState(initValues)

    const handleChange = e => {
        setValues(prev => ({
            ...prev,
            [e.target.name]: e.target.value
        }))
    }

    async function handleSubmit(event) {
        event.preventDefault()

        try {
            await client.resetStore()
            const { data } = await signIn({
                variables: values,
            })
            if (data.login.user) {
                await router.push('/transport')
                enqueueSnackbar('Success', { variant: 'success' });
            }
        } catch (error) {
            enqueueSnackbar(getErrorMessage(error), { variant: 'error' });
        }
    }

    return {
        onSetValues: setValues,
        onChange: handleChange,
        onSubmit: handleSubmit,
        values,
        loading
    }
}

export default useHomeForm